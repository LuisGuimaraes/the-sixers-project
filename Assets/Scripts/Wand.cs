using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

using PathCreation.Examples;

public class Wand : MonoBehaviour, NonCollidable
{
    [SerializeField]
    private GameObject _spellPrefab;
    [SerializeField]
    private FMODUnity.EventReference _spellIdle;
    [SerializeField]
    private FMODUnity.EventReference _spellShoot;

    [SerializeField]
    private GameObject _tip;

    [SerializeField]
    private PathFollower pathFollower;
    private bool _canCast = false;

    [SerializeField]
    private bool _alwaysCast = false;

    private GameObject _currentSpell;

    public bool spellTraced = false;

    public bool _deleteLater = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (_deleteLater)
        {
            ShootSpell();
            _deleteLater = false;
        }
    }

    public void EnableSpellCast()
    {
        if (_canCast)
        {
            return;
        }
        _canCast = true;
        InstantiateSpell();
    }

    private void InstantiateSpell()
    {
        _currentSpell = Instantiate(_spellPrefab, _tip.transform.position, Quaternion.identity);
        _currentSpell.transform.parent = _tip.transform;
        FMODUnity.RuntimeManager.PlayOneShotAttached(_spellIdle, _currentSpell.gameObject);
    }

    public void ShootSpell()
    {
        if (_alwaysCast || _canCast)
        {
            if (_currentSpell == null)
            {
                InstantiateSpell();
            }
            Fireball fireball = _currentSpell.GetComponent<Fireball>();
            fireball.Shoot(_tip.transform.forward);
            _currentSpell.transform.parent = null;

            pathFollower.ShowNextSpell();
            _canCast = false;
            _currentSpell = null;

            FMODUnity.RuntimeManager.PlayOneShotAttached(_spellShoot, fireball.gameObject);
        }
    }
}
