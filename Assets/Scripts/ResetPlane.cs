using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPlane : MonoBehaviour, NonCollidable
{

    [SerializeField]
    private ResetPlaneGroup _group;
    // Start is called before the first frame update
    public void OnTriggerEnter(Collider other)
    {
        _group.OnTriggerEnter(other);
    }
}
