using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformData
{
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 scale;

    public TransformData(Vector3 position, Quaternion rotation, Vector3 scale) { 
        this.position = position;
        this.rotation = rotation;
        this.scale = scale;
    }
}
public class ResetPlaneGroup : MonoBehaviour, NonCollidable
{

    [SerializeField]
    private List<GameObject> _trackedObjects = new List<GameObject>();

    private Dictionary<GameObject, TransformData> _trackedObjectsInitialPosition = new Dictionary<GameObject, TransformData>();
    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject gameObject in _trackedObjects)
        {
            TransformData data = new TransformData(
                gameObject.transform.position,
                gameObject.transform.rotation,
                gameObject.transform.localScale
                );
            _trackedObjectsInitialPosition.Add(gameObject, data);
        }
    }

    private void ResetObject(GameObject gameObject)
    {
        TransformData data = _trackedObjectsInitialPosition[gameObject];
        if (data != null)
        {
            gameObject.transform.position = data.position;
            gameObject.transform.rotation = data.rotation;
            gameObject.transform.localScale = data.scale;

            Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();
            if (rigidbody != null)
            {
                rigidbody.velocity = Vector3.zero;
            }

        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (_trackedObjects.Contains(other.gameObject))
        {
            ResetObject(other.gameObject);
        }
    }
}
