using PathCreation.Examples;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Content.Interaction;

public class WandTip : MonoBehaviour, NonCollidable
{
    [SerializeField]
    private FMODUnity.EventReference _wandOnPath;
    private FMOD.Studio.EventInstance _wandOnPathInstance;

    public PathFollower pathFollower;
    [SerializeField]
    private GameObject _sphere;

    [SerializeField]
    private List<Material> _materials;

    private void Start()
    {
        _wandOnPathInstance = FMODUnity.RuntimeManager.CreateInstance(_wandOnPath);
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(_wandOnPathInstance, gameObject.transform);
        _wandOnPathInstance.setParameterByName("CorrectPathVolume", 0);
        _wandOnPathInstance.start();
    }
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.GetComponent<TrackingSphere>() != null)
        {
            pathFollower.speed = pathFollower.maxSpeed;
            _wandOnPathInstance.setParameterByName("CorrectPathVolume", 1);
            Debug.Log("Sound");
            _sphere.GetComponent<MeshRenderer>().material = _materials[0];
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.GetComponent<TrackingSphere>() != null)
        {
            pathFollower.speed = -pathFollower.returnSpeed;
            //_wandOnPathInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            _wandOnPathInstance.setParameterByName("CorrectPathVolume", 0);
            _sphere.GetComponent<MeshRenderer>().material = _materials[1];
        }
    }
}
