using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using PathCreation;

public class Spells : MonoBehaviour
{
    public List<GameObject> spells;

    // Start is called before the first frame update
    void Start()
    {

    }

    public PathCreator getPathCreator(int idx)
    {
        return spells [idx].gameObject.GetComponent<PathCreator>();
    }

    public GameObject getObject(int idx)
    {
        return spells[idx];
    }

    public List<GameObject> getSpellList()
    {
        return spells;
    }

    // Update is called once per frames
    void Update()
    {
        
    }
}
