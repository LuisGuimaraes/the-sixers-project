using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fireball : MonoBehaviour
{
    private Rigidbody _rigidBody;

    [SerializeField]
    private bool _activated = false;

    [SerializeField]
    private float _force;

    [SerializeField]
    private float _range = 20.0f;

    [SerializeField]
    private float _explosionRadius = 1.0f;

    [SerializeField]
    private GameObject _explosionPrefab;

    [SerializeField]
    private FMODUnity.EventReference _explosionSound;

    private Vector3 _initialPosition;

    [SerializeField]
    private LayerMask _layerMaskToIgnore;

    // Start is called before the first frame update
    void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(_initialPosition, transform.position);
        if (distance > _range)
        {
            DestroySpell();
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<NonCollidable>() != null)
        {
            return;
        }
        if (_activated)
        {
            Debug.Log("Collided with " + other.gameObject.name);
            Explode();
        }
    }

    public void Shoot(Vector3 direction)
    {
        _rigidBody.AddForce(direction * _force * Time.deltaTime * 1000.0f);
        _activated = true;
    }

    private void Explode()
    {
        DestroySpell();
        RaycastHit[] hits = Physics.SphereCastAll(
            transform.position,
            _explosionRadius,
            Vector3.up,
            0.0f,
            ~_layerMaskToIgnore
        );

        foreach (RaycastHit hit in hits)
        {
            GameObject other = hit.collider.gameObject;
            Rigidbody rigidbody = other.GetComponent<Rigidbody>();

            if (other == gameObject)
            {
                continue;
            }
            if (rigidbody != null)
            {
                rigidbody.AddExplosionForce(
                    _force,
                    transform.position,
                    _explosionRadius,
                    0.0f,
                    ForceMode.Impulse
                );
            }
        }

        GameObject explosion = Instantiate(
            _explosionPrefab,
            transform.position,
            Quaternion.identity
        );
        FMODUnity.RuntimeManager.PlayOneShotAttached(_explosionSound, gameObject);

        Destroy(explosion, 2.0f);
    }

    private void DestroySpell()
    {
        Destroy(gameObject);
    }
}
