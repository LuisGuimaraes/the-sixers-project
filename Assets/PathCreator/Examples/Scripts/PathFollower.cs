﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.EventSystems;

namespace PathCreation.Examples
{
    // Moves along a path at constant speed.
    // Depending on the end of path instruction, will either loop, reverse, or stop at the end of the path.
    public class PathFollower : MonoBehaviour
    {
        public Wand wand;
        [SerializeField]
        private Spells spells;
        public PathCreator pathCreator;

        public EndOfPathInstruction endOfPathInstruction;
        public float speed = 0;
        public float maxSpeed = 1;
        public float returnSpeed = 0.1f;
        float distanceTravelled;

        int currentSpell;

        GameObject activeSpellObject;

        [SerializeField]
        private GameObject vfx;

        void Start() {
            speed = 0;
            currentSpell = 0;

            activeSpellObject = Instantiate(spells.getObject(currentSpell), spells.transform);
            activeSpellObject.transform.parent = spells.transform;
            pathCreator = activeSpellObject.GetComponent<PathCreator>();

#if UNITY_EDITOR
            Selection.activeGameObject = activeSpellObject;
#endif


            if (pathCreator != null)
            {
                // Subscribed to the pathUpdated event so that we're notified if the path changes during the game
                pathCreator.pathUpdated += OnPathChanged;
            }
        }

        void Update()
        {
            if (pathCreator != null)
            {
                distanceTravelled += speed * Time.deltaTime;

                transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);
                transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);

                vfx.transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled + 0.1f, endOfPathInstruction);
                vfx.transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);

                if (pathCreator.path.GetClosestTimeOnPath(transform.position) >= 0.98)
                {

                    Destroy(activeSpellObject);

                    currentSpell++;
                    if (currentSpell >= spells.getSpellList().Count)
                    {
                        currentSpell = 0;
                    }

                    wand.EnableSpellCast();
                }
            }
        }

        public void ShowNextSpell()
        {
            activeSpellObject = Instantiate(spells.getObject(currentSpell), spells.transform);
            activeSpellObject.transform.parent = spells.transform;
            pathCreator = activeSpellObject.GetComponent<PathCreator>();

#if UNITY_EDITOR
            Selection.activeGameObject = activeSpellObject;
#endif

            // Reset pos
            distanceTravelled = 0;
            transform.position = pathCreator.path.GetPointAtDistance(0, endOfPathInstruction);
            transform.rotation = pathCreator.path.GetRotationAtDistance(0, endOfPathInstruction);
        }

        // If the path changes during the game, update the distance travelled so that the follower's position on the new path
        // is as close as possible to its position on the old path
        void OnPathChanged() {
            distanceTravelled = pathCreator.path.GetClosestDistanceAlongPath(transform.position);
        }
    }
}
