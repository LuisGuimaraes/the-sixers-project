# A Serious Game for Parkinson's Patients Using VR Technology



## Template for Unity XR Interaction Toolkit Project

Based on this repository. [https://github.com/Unity-Technologies/XR-Interaction-Toolkit-Examples](https://github.com/Unity-Technologies/XR-Interaction-Toolkit-Examples)

## Required Setup

- Unity 2022.3.x LTS
- Unity XR Interaction Toolkit Package

## Demo
See [this](demo.mp4).

## Details
See the [report](report.pdf).